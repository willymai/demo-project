import React from 'react';
import {CheckBox} from 'react-native-elements';

export default function StyledCheckbox({checked, title, onPress, ...rest}) {
  return (
    <CheckBox
      title={title}
      checked={checked}
      onPress={onPress}
      containerStyle={{
        padding: 0,
        backgroundColor: 'transparent',
        borderWidth: 0,
        marginLeft: 0,
        marginRight: 0,
      }}
      {...rest}
    />
  );
}
