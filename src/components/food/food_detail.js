import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ImageHeaderScrollView} from 'react-native-image-header-scroll-view';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import Food1 from '../../assets/images/food/milk-tea.png';
import Colors from '../../constants/colors';
import Icons from '../../constants/icons';
import {FoodDetailData} from '../../constants/mock_data';
import OutlineButton from '../button/outline_button';
import StyledCheckbox from '../checkbox/styled_checkbox';
import Topping1 from '../../assets/images/food/topping-1.png';
import StyledInput from '../input/styled_input';

const Data = [
  {
    image: Food1,
  },
  {
    image: Food1,
  },
  {
    image: Food1,
  },
];

export default function FoodDetail() {
  const [activeIndex, setActiveIndex] = useState(0);

  // const [totalPrice, setTotalPrice] = useState(FoodDetailData.price);

  const [quantity, setQuantity] = useState(1);

  const [selectedToppings, setSelectedToppings] = useState([]);

  const [selectedSize, setSelectedSize] = useState(0);

  const [selectedSugar, setselectedSugar] = useState();

  const renderItem = ({item, index}) => {
    return (
      <View style={{backgroundColor: 'red'}}>
        <Image
          source={item.image}
          resizeMode="cover"
          style={{width: viewportWidth}}
        />
      </View>
    );
  };

  const {width: viewportWidth} = Dimensions.get('screen');

  const handleSelectTopping = topping => {
    let newToppings = [...selectedToppings];

    const index = selectedToppings.findIndex(
      item => item.type === topping.type,
    );

    if (index !== -1) {
      newToppings.splice(index, 1);
    } else {
      newToppings.push(topping);
    }
    setSelectedToppings(newToppings);
  };

  return (
    <View style={styles.container}>
      <ImageHeaderScrollView
        scrollViewBackgroundColor={Colors.backgroundColor}
        maxHeight={280}
        minHeight={0}
        renderHeader={() => (
          <View
            style={{
              height: 300,
            }}>
            <Carousel
              autoplay
              loop
              data={Data}
              renderItem={renderItem}
              sliderWidth={viewportWidth}
              itemWidth={viewportWidth}
              onSnapToItem={setActiveIndex}
              inactiveSlideScale={1}
            />
            <View style={styles.dotWrapper}>
              <Pagination
                dotsLength={Data.length}
                activeDotIndex={activeIndex}
                containerStyle={{paddingTop: 0, marginTop: -16}}
                inactiveDotScale={1}
                dotContainerStyle={{
                  marginHorizontal: 4,
                }}
                inactiveDotOpacity={1}
                inactiveDotColor="white"
                dotColor="#FBA808"
                dotStyle={{
                  width: 6,
                  height: 6,
                }}
              />
            </View>
          </View>
        )}>
        <View
          style={{
            height: '100%',
            padding: 20,
            backgroundColor: Colors.backgroundColor,
            paddingBottom: 100,
          }}>
          {/* Header */}
          <View style={styles.foodHeader}>
            <View>
              <Text style={styles.foodName}>{FoodDetailData.name}</Text>
              <Text style={styles.foodPrice}>{FoodDetailData.price}đ</Text>
            </View>
            <View style={{height: 46, alignItems: 'center', paddingTop: 5}}>
              <Image source={Icons.icLove} />
              <Text style={styles.loveText}>Yêu thích</Text>
            </View>
          </View>

          {/* Topping */}
          <View
            style={{paddingTop: 16, paddingBottom: 24, marginHorizontal: -20}}>
            <ScrollView horizontal>
              <View style={{width: 20}} />
              {FoodDetailData.topping.map(item => {
                const isSelected =
                  selectedToppings.findIndex(
                    topping => topping.type === item.type,
                  ) !== -1;
                return (
                  <View key={item.key}>
                    <View style={styles.toppingWrapper}>
                      <TouchableOpacity
                        onPress={() => handleSelectTopping(item)}>
                        <View
                          style={[
                            styles.topping,
                            isSelected ? {borderColor: Colors.mainRed} : {},
                          ]}>
                          <View style={{paddingRight: 8}}>
                            <Image source={Topping1} />
                          </View>
                          <View>
                            <Text style={styles.toppingName}>{item.name}</Text>
                            <Text style={styles.toppingPrice}>
                              +{item.price}đ
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </View>

          {/* Description */}
          <View style={{paddingBottom: 16}}>
            <Text style={styles.foodDesc}>{FoodDetailData.description}</Text>
          </View>

          {/* Size */}
          <View style={{paddingBottom: 16}}>
            <View style={styles.cardWrapper}>
              <View style={styles.cardHeader}>
                <View>
                  <Text style={styles.cardTitle}>Size</Text>
                  <Text style={styles.cardSubTitle}>Chọn 1 loại size</Text>
                </View>
                <View>
                  <OutlineButton text="#Bắt buộc" color={Colors.mainRed} />
                </View>
              </View>

              <View style={{paddingTop: 10}}>
                {FoodDetailData.size.map(size => (
                  <View key={size.price}>
                    <StyledCheckbox
                      checked={selectedSize === size.price}
                      iconType="material"
                      checkedColor={Colors.mainRed}
                      checkedIcon="radio-button-checked"
                      uncheckedColor={Colors.text.regular}
                      uncheckedIcon="radio-button-off"
                      title={
                        <View style={styles.checkboxText}>
                          <Text style={styles.checkboxTextName}>
                            {size.name}
                          </Text>
                          <Text style={styles.checkboxTextPrice}>
                            +{size.price}đ
                          </Text>
                        </View>
                      }
                      onPress={() => setSelectedSize(size.price)}
                    />
                  </View>
                ))}
              </View>
            </View>
          </View>

          {/* Sugar */}
          <View style={{paddingBottom: 16}}>
            <View style={styles.cardWrapper}>
              <View style={styles.cardHeader}>
                <View>
                  <Text style={styles.cardTitle}>Lượng đường</Text>
                  <Text style={styles.cardSubTitle}>Chọn độ ngọt</Text>
                </View>
                <View>
                  <OutlineButton text="#Tùy chọn" color="#F4A34A" />
                </View>
              </View>

              <View style={{paddingTop: 10}}>
                {FoodDetailData.sugar.map(size => (
                  <View key={size.price}>
                    <StyledCheckbox
                      checked={selectedSugar === size.percent}
                      iconType="material"
                      checkedColor={Colors.mainRed}
                      checkedIcon="radio-button-checked"
                      uncheckedColor={Colors.text.regular}
                      uncheckedIcon="radio-button-off"
                      title={
                        <View style={styles.checkboxText}>
                          <Text style={styles.checkboxTextName}>
                            {size.percent}% đường
                          </Text>
                          <Text style={styles.checkboxTextPrice}>+0đ</Text>
                        </View>
                      }
                      onPress={() => setselectedSugar(size.percent)}
                    />
                  </View>
                ))}
              </View>
            </View>
          </View>

          {/* Note */}
          <View style={{paddingBottom: 16}}>
            <View style={styles.cardWrapper}>
              <View style={styles.cardHeader}>
                <View>
                  <Text style={styles.cardTitle}>Note</Text>
                  <Text style={styles.cardSubTitle}>
                    Để lại lời nhắn cho cửa hàng
                  </Text>
                </View>
              </View>

              <View style={{paddingTop: 10}}>
                <StyledInput placeholder="Any pickup notes?" />
              </View>
            </View>
          </View>
          {/* Note */}

          {/* Quantity */}
          {/* Quantity */}
        </View>
      </ImageHeaderScrollView>
      {/* Total */}
      <View style={styles.totalWrapper}>
        <View style={[styles.row, styles.total]}>
          <View>
            <Text style={styles.totalName}>{FoodDetailData.name}</Text>
            <Text style={styles.totalPrice}>
              {quantity *
                (FoodDetailData.price +
                  selectedSize +
                  selectedToppings.reduce((acc, cur) => acc + cur.price, 0))}
            </Text>
          </View>
          <View>
            <TouchableOpacity>
              <View style={styles.orderButton}>
                <Text style={styles.orderText}>Chọn món</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {/*  */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: Colors.backgroundColor,
  },
  dotWrapper: {},
  foodHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 4,
  },
  foodName: {
    fontSize: 24,
    lineHeight: 36,
    color: Colors.text.dark,
    fontWeight: '500',
  },
  foodPrice: {
    fontSize: 18,
    lineHeight: 27,
    color: Colors.text.regular,
    paddingTop: 8,
  },
  loveText: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.text.regular,
    paddingTop: 8,
  },
  toppingWrapper: {
    marginRight: 18,
  },
  topping: {
    paddingVertical: 14,
    paddingHorizontal: 12,
    backgroundColor: 'white',
    borderRadius: 10,
    width: 185,
    borderWidth: 1,
    borderColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  toppingName: {
    fontSize: 12,
    lineHeight: 14,
  },
  toppingPrice: {
    fontSize: 12,
    lineHeight: 14,
    paddingTop: 3,
    color: Colors.text.regular,
  },
  foodDesc: {
    fontSize: 16,
    lineHeight: 24,
    color: Colors.text.regular,
  },
  cardWrapper: {
    backgroundColor: '#FFF',
    padding: 24,
    borderRadius: 10,
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardTitle: {
    fontSize: 24,
    lineHeight: 36,
    color: Colors.text.dark,
  },
  cardSubTitle: {
    lineHeight: 21,
    color: Colors.text.regular,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  checkboxText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexGrow: 1,
    paddingLeft: 12,
  },
  checkboxTextName: {
    color: Colors.text.dark,
  },
  checkboxTextPrice: {
    color: Colors.text.regular,
  },
  totalName: {
    fontSize: 16,
    color: '#FFF',
  },
  total: {
    backgroundColor: Colors.mainRed,
    padding: 20,
    borderRadius: 10,
    fontWeight: '500',
  },
  totalPrice: {
    fontSize: 14,
    color: '#FFF',
    paddingTop: 5,
  },
  totalWrapper: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 20,
  },
  orderButton: {
    paddingVertical: 12,
    paddingHorizontal: 15,
    backgroundColor: '#FFF',
    borderRadius: 10,
  },
});
