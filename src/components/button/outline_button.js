import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Colors from '../../constants/colors';

export default function OutlineButton({onPress, text, color}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.button, {borderColor: color}]}>
        <Text style={[styles.text, {color: color}]}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 6,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: Colors.text.regular,
    borderRadius: 10,
  },
  text: {
    fontSize: 12,
    lineHeight: 21,
    color: Colors.text.regular,
  },
});
