import React from 'react';
import {Image, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import Modal from 'react-native-modal';
import Colors from '../../constants/colors';
import Icons from '../../constants/icons';
import FoodDetail from '../food/food_detail';

export default function ModalFoodDetail({onClose, isVisible}) {
  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={onClose}
      backdropColor="rgba(0,0,0,0.2)"
      style={[styles.modal]}>
      <View style={[styles.content]}>
        <View style={[styles.close, styles.iconWrapper]}>
          <TouchableWithoutFeedback
            onPress={onClose}
            containerStyle={styles.iconWrapper}>
            <Image source={Icons.icClose} />
          </TouchableWithoutFeedback>
        </View>
        <View style={{borderRadius: 15, overflow: 'hidden', flexGrow: 1}}>
          <FoodDetail />
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    marginTop: 80,
  },
  content: {
    height: '100%',
    backgroundColor: Colors.backgroundColor,
    borderRadius: 15,

    shadowColor: 'rgba(18, 39, 61, 0.12)',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    borderStyle: 'solid',
    // borderWidth: 1,
    // borderColor: '#e7ebef',
    position: 'relative',
  },
  close: {
    position: 'absolute',
    top: 14,
    right: 14,
    zIndex: 10,
  },
  iconWrapper: {
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconClose: {
    color: Colors.mainRed,
    fontSize: 16,
  },
});
