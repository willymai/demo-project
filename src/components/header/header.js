import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';

import Icons from '../../constants/icons';
import Logo from '../../assets/images/logo.png';

export default function Header() {
  return (
    <View
      style={[styles.container, {paddingTop: DeviceInfo.hasNotch() ? 30 : 20}]}>
      <StatusBar barStyle="light-content" />
      <View style={styles.inner}>
        <Image
          source={Logo}
          style={{height: 28, width: 100}}
          resizeMode="contain"
        />
        <Image source={Icons.icCreditCard} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
  },

  inner: {
    paddingHorizontal: 24,
    paddingVertical: 13,
    paddingTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  text: {
    color: '#FFF',
    textTransform: 'uppercase',
    fontSize: 24,
    lineHeight: 28,
    fontWeight: 'bold',
  },
});
