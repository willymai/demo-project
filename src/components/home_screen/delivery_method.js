import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../constants/colors';
import ScrollViewTab from '../tab/scroll_view_tab';
const Tabs = [
  {
    title: 'GIAO TẬN NƠI',
    value: 'delivery',
  },
  {
    title: 'TỰ ĐẾN LẤY',
    value: 'takeaway',
  },
];
export default function DeliveryMethod() {
  const [method, setMethod] = useState('delivery');

  const renderMethodContent = () => {
    if (method === 'takeaway') return <Text>On develop</Text>;
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text>ĐẾN ĐỊA CHỈ</Text>
          <TouchableOpacity>
            <View style={styles.changeTextWrapper}>
              <Text style={styles.changeText}>ĐỔI</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{paddingTop: 8}}>
          <Text style={styles.address} numberOfLines={1}>
            279 Đường Cách Mạng Tháng 8, Phường 12, Quận 10, Tp Hồ Chí Minh
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={{paddingBottom: 20}}>
      <View style={styles.container}>
        <View style={styles.tabs}>
          <ScrollViewTab
            isScroll={false}
            currentTab={method}
            onPress={item => setMethod(item.value)}
            tabs={Tabs}
            tabItemWrapperStyle={{
              marginRight: 0,
              flexGrow: 1,
            }}
            tabItemStyle={{
              paddingBottom: 15,
              paddingTop: 15,
              alignItems: 'center',
            }}
            textStyle={{
              fontSize: 14,
              lineHeight: 21,
              fontWeight: '500',
            }}
            tabItemLineStyle={{
              height: 2,
            }}
          />
        </View>
        <View style={styles.content}>{renderMethodContent()}</View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 10,
  },
  tabs: {},
  content: {
    padding: 16,
  },
  changeTextWrapper: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.mainRed,
  },
  changeText: {
    color: Colors.mainRed,
  },
  address: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.text.regular,
  },
});
