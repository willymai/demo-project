import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CateImage1 from '../../assets/images/category/breakfast.png';
import CateImage2 from '../../assets/images/category/rice.png';
import CateImage3 from '../../assets/images/category/super-deal.png';
import CateImage4 from '../../assets/images/category/drink.png';
import CateImage5 from '../../assets/images/category/freeship.png';
import CateImage6 from '../../assets/images/category/fast-food.png';
import CateImage7 from '../../assets/images/category/lunch.png';
import CateImage8 from '../../assets/images/category/more.png';
import ContentBlock from '../content_block';

const Data = [
  {
    title: 'Breakfast',
    image: CateImage1,
  },
  {
    title: 'Rice',
    image: CateImage2,
  },
  {
    title: 'Super Deal',
    image: CateImage3,
  },
  {
    title: 'Drink',
    image: CateImage4,
  },
  {
    title: 'Freeship',
    image: CateImage5,
  },
  {
    title: 'Fast Food',
    image: CateImage6,
  },
  {
    title: 'Lunch',
    image: CateImage7,
  },
  {
    title: 'More',
    image: CateImage8,
  },
];

export default function Category({onPress}) {
  return (
    <ContentBlock
      title="Categories"
      actionText="See all"
      showAction
      onActionPress>
      <View style={styles.list}>
        {Data.map(item => (
          <View key={item.title} style={styles.categoryItemWrapper}>
            <TouchableOpacity onPress={onPress}>
              <View style={styles.categoryItem}>
                <View style={styles.categoryImage}>
                  <Image source={item.image} />
                </View>
                <Text style={styles.categoryTitle}>{item.title}</Text>
              </View>
            </TouchableOpacity>
          </View>
        ))}
      </View>
    </ContentBlock>
  );
}

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: -9,
  },
  categoryItemWrapper: {
    // flexBasis: '25%',
    width: '25%',
    paddingHorizontal: 9,
    paddingBottom: 20,
    // marginHorizontal: 9,
  },
  categoryItem: {
    alignItems: 'center',
    // marginBottom: 20,
  },
  categoryImage: {
    flexGrow: 1,
    width: '100%',
    // width: 66,
    height: 66,
    backgroundColor: '#FFF',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  categoryTitle: {
    fontSize: 12,
    lineHeight: 16,
    paddingTop: 8,
  },
});
