import React from 'react';
import ContentBlock from '../content_block';
import VoucherImage from '../../assets/images/voucher.png';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

export default function Voucher({onPress}) {
  return (
    <ContentBlock title="Voucher" actionText="See all" showAction onActionPress>
      <View style={{marginRight: -20, marginLeft: -20}}>
        <ScrollView horizontal>
          <View style={{width: 20}} />
          {Array(5)
            .fill(null)
            .map((item, index) => (
              <View key={index} style={styles.voucherWrapper}>
                <TouchableOpacity onPress={onPress}>
                  <Image source={VoucherImage} />
                </TouchableOpacity>
              </View>
            ))}
        </ScrollView>
      </View>
    </ContentBlock>
  );
}

const styles = StyleSheet.create({
  voucherWrapper: {
    marginRight: 16,
  },
  voucher: {},
});
