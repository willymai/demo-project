import React, {useState} from 'react';
import {Image, View} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import Banner1 from '../../assets/images/banner/banner-1.png';

const Data = [
  {
    image: Banner1,
  },
  {
    image: Banner1,
  },
  {
    image: Banner1,
  },
];

export default function ListBanner() {
  const [activeIndex, setActiveIndex] = useState(0);
  const renderItem = ({item, index}) => {
    return (
      <View key={index} style={{width: 320}}>
        <Image source={item.image} />
      </View>
    );
  };

  return (
    <View style={{marginLeft: -20, marginRight: -20}}>
      <View style={{flexDirection: 'row'}}>
        <Carousel
          loop
          data={Data}
          renderItem={renderItem}
          sliderWidth={340}
          itemWidth={340}
          onSnapToItem={setActiveIndex}
          containerCustomStyle={{
            paddingLeft: 20,
          }}
          inactiveSlideScale={1}
        />
      </View>
      <Pagination
        dotsLength={Data.length}
        activeDotIndex={activeIndex}
        containerStyle={{paddingTop: 12}}
        inactiveDotScale={1}
        dotContainerStyle={{
          marginHorizontal: 4,
        }}
        dotStyle={{
          width: 4,
          height: 4,
        }}
      />
    </View>
  );
}
