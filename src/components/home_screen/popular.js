import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import ContentBlock from '../content_block';

import ImageRice from '../../assets/images/rice.png';
import ImageNoodle from '../../assets/images/noodle.png';

const Data = [
  {
    id: 1,
    title: 'Rice',
    image: ImageNoodle,
  },
  {
    id: 2,
    title: 'Noodle',
    image: ImageNoodle,
  },
  {
    id: 3,
    title: 'Rice',
    image: ImageNoodle,
  },
];

export default function Popular({onPress}) {
  return (
    <ContentBlock
      title="Poppular"
      actionText="See all"
      showAction
      onActionPress>
      <View style={{marginRight: -20, marginLeft: -20}}>
        <ScrollView horizontal>
          <View style={{width: 20}} />
          {Data.map(item => (
            <View key={item.id} style={styles.foodWrapper}>
              <TouchableOpacity onPress={onPress}>
                <View style={styles.food}>
                  <Image source={item.image} />
                  <Text>{item.title}</Text>
                </View>
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      </View>
    </ContentBlock>
  );
}

const styles = StyleSheet.create({
  foodWrapper: {
    marginRight: 16,
  },
  food: {
    paddingTop: 11,
    paddingBottom: 16,
    // paddingHorizontal: 20,
    backgroundColor: '#FFF',
    width: 126,
    height: 153,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
