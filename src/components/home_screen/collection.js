import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ContentBlock from '../content_block';
import CollectionImage1 from '../../assets/images/collection-1.png';
import CollectionImage2 from '../../assets/images/collection-2.png';
const Data = [
  {
    id: 1,
    title: 'Super Deal',
    image: CollectionImage1,
  },
  {
    id: 2,
    title: 'Freeship',
    image: CollectionImage2,
  },
  {
    id: 3,
    title: 'Buy one get one',
    image: CollectionImage1,
  },
];

export default function Collection({onPress}) {
  return (
    <ContentBlock
      title="Collection"
      actionText="See all"
      showAction
      onActionPress>
      <View style={{marginRight: -20, marginLeft: -20}}>
        <ScrollView horizontal>
          <View style={{width: 20}} />
          {Data.map(item => (
            <View style={styles.itemWrapper} key={item.title}>
              <TouchableOpacity onPress={onPress}>
                <View style={styles.item}>
                  <View>
                    <Image source={item.image} />
                  </View>
                  <View style={styles.textWrapper}>
                    <Text>{item.title}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      </View>
    </ContentBlock>
  );
}

const styles = StyleSheet.create({
  itemWrapper: {
    marginRight: 16,
  },
  textWrapper: {
    paddingVertical: 18,
    paddingHorizontal: 12,
    backgroundColor: '#FFF',
  },
});
