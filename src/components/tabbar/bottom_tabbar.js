import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {
  ACCOUNT_TAB_LABEL,
  HOME_TAB_LABEL,
  ORDER_TAB_LABEL,
  STORE_TAB_LABEL,
  WALLET_TAB_LABEL,
} from '../../constants/routing';
import Colors from '../../constants/colors';
import Icons from '../../constants/icons';
export default function BottomTabbar({state, navigation}) {
  console.log('state', state);
  return (
    <View style={styles.container}>
      <View style={styles.tabbar}>
        {state.routes.map((route, index) => {
          let label = null;
          let icon = null;

          const isFocused = state.index === index;

          const tintColor = isFocused ? Colors.mainRed : Colors.text.regular;

          switch (route.name) {
            case HOME_TAB_LABEL:
              label = 'Trang Chủ';
              icon = Icons.icHome;
              break;
            case ORDER_TAB_LABEL:
              label = 'Đặt Món';
              icon = Icons.icOrder;
              break;
            case WALLET_TAB_LABEL:
              label = 'Topup';
              icon = Icons.icWallet;
              break;
            case STORE_TAB_LABEL:
              label = 'Cửa Hàng';
              icon = Icons.icStore;
              break;
            case ACCOUNT_TAB_LABEL:
              label = 'Tài Khoản';
              icon = Icons.icUser;
              break;

            default:
              break;
          }
          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          return (
            <TouchableOpacity key={index} onPress={onPress}>
              <View style={styles.tabbarItem}>
                <Image
                  source={icon}
                  style={{width: 18, height: 18, tintColor: tintColor}}
                />
                <Text style={[styles.text, {color: tintColor}]}>{label}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 12,
    paddingVertical: 16,
    backgroundColor: '#FFF',
  },
  tabbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: Colors.text.regular,
    fontSize: 11,
    fontWeight: '500',
    lineHeight: 18,
    paddingTop: 10,
  },
  tabbarItem: {
    alignItems: 'center',
    paddingHorizontal: 10,
  },
});
