import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../constants/colors';

export default function ContentBlock({
  title,
  showAction,
  actionText,
  onActionPress,
  children,
}) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
        {showAction && (
          <TouchableOpacity>
            <View>
              <Text style={styles.actionText}>{actionText}</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.body}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 30,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 18,
    color: Colors.text.dark,
    lineHeight: 21,
    fontWeight: '500',
  },
  actionText: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.mainRed,
  },
  body: {
    paddingTop: 20,
  },
});
