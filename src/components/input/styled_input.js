import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import Colors from '../../constants/colors';

export default function StyledInput({
  inputRef,
  placeholder,
  placeholderTextColor,
  prefix,
  prefixStyle,
  value,
  secureTextEntry,
  keyboardType,
  returnKeyType,
  keyboardAppearance,
  onFocus,
  onBlur,
}) {
  return (
    <View style={styles.container}>
      <View style={styles.inputWrapper}>
        {prefix && <View style={[styles.prefix, prefixStyle]}>{prefix}</View>}
        <View style={[styles.inputContent]}>
          <TextInput
            ref={inputRef}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor || Colors.text.regular}
            style={[styles.inputStyle]}
            value={value}
            secureTextEntry={secureTextEntry}
            keyboardType={keyboardType}
            returnKeyType={returnKeyType}
            keyboardAppearance={keyboardAppearance}
            onFocus={onFocus}
            onBlur={onBlur}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
  },
  inputWrapper: {
    backgroundColor: Colors.inputBackgroundColor,
    borderRadius: 10,
    height: 48,
    flexDirection: 'row',
  },
  prefix: {
    minWidth: 28,
    alignItems: 'center',
    justifyContent: 'center',
    // borderRightColor: colors.inputBorderColor,
  },
  inputContent: {
    // paddingVertical: 8,
    // paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    // height: 36
  },
  inputStyle: {
    width: '100%',
    height: '100%',
    fontSize: 14,
    paddingHorizontal: 12,
  },
});
