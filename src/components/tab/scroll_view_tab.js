import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Colors from '../../constants/colors';

/**
 *
 * @param {
 *  value: Any,
 *  title: String
 * } tabs
 */
export default function ScrollViewTab({
  tabs,
  onPress,
  currentTab,
  tabItemStyle,
  containerStyle,
  innerStyle,
  tabItemWrapperStyle,
  activeTabColor,
  textStyle,
  tabLineStyle,
  tabItemLineStyle,
  scrollEnabled = true,
  isScroll = true,
  listTabStyle,
}) {
  const content = tabs.map(tab => (
    <View key={tab.value} style={[styles.tabItemWrapper, tabItemWrapperStyle]}>
      <TouchableOpacity key={tab.value} onPress={() => onPress(tab)}>
        <View style={[styles.tabItem, tabItemStyle]}>
          <Text
            style={[
              {
                fontSize: 14,
                color:
                  currentTab === tab.value
                    ? activeTabColor || Colors.mainRed
                    : '#2F2F2F',
              },
              textStyle,
            ]}>
            {tab.title}
          </Text>
          <View
            style={[
              {
                height: 1,
                left: 0,
                right: 0,
                flexGrow: 1,
                backgroundColor:
                  currentTab === tab.value ? Colors.mainRed : 'transparent',
                position: 'absolute',
                bottom: 0,
                zIndex: 10,
              },
              tabItemLineStyle,
            ]}
          />
        </View>
      </TouchableOpacity>
    </View>
  ));

  const renderContent = () => {
    if (isScroll)
      return (
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEnabled={scrollEnabled}>
          {content}
        </ScrollView>
      );
    return <View style={[styles.listTab, listTabStyle]}>{content}</View>;
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={innerStyle}>{renderContent()}</View>
      <View style={[styles.tabLine, tabLineStyle]} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  tabItem: {
    paddingBottom: 10,
    position: 'relative',
  },
  tabItemWrapper: {
    marginRight: 15,
  },
  tabLine: {
    width: '100%',
    height: 1,
    top: -1,
    backgroundColor: '#F2F2F2',
    zIndex: -1,
  },
  listTab: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
