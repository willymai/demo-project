const Colors = {
  text: {
    dark: '#01000D',
    regular: '#8F92A1',
  },
  mainRed: '#EC1B23',
  // backgroundColor: '#E5E5E5',
  backgroundColor: 'rgb(247, 248, 249)',
  inputBackgroundColor: 'rgba(143, 146, 161, 0.08)',
};

export default Colors;
