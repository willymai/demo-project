export const FoodDetailData = {
  name: 'Hồng trà sữa',
  price: 45000,
  images: [],
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis, sed ullamcorper turpis non nunc. Et id pellentesqu Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis, sed ullamcorper turpis non nunc. Et id pellentesqu., Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis, sed ullamcorper turpis non nunc. Et id pellentesqu,',
  topping: [
    {
      name: 'Trân châu đen',
      type: 'black_bubble',
      price: 5000,
    },
    {
      name: 'Trân châu trắng',
      type: 'white_bubble',
      price: 5000,
    },
  ],
  size: [
    {
      name: 'Nhỏ',
      price: 0,
    },
    {
      name: 'Vừa',
      price: 7000,
    },
    {
      name: 'Lớn',
      price: 14000,
    },
  ],
  sugar: [
    {
      percent: 0,
    },
    {
      percent: 20,
    },
    {
      percent: 50,
    },
    {
      percent: 70,
    },
    {
      percent: 100,
    },
  ],
};
