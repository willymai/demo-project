const Routing = {};

export const HOME_TAB_LABEL = 'HOME';
export const ORDER_TAB_LABEL = 'ORDER';
export const WALLET_TAB_LABEL = 'WALLET';
export const STORE_TAB_LABEL = 'STORE';
export const ACCOUNT_TAB_LABEL = 'ACCOUNT';

export default Routing;
