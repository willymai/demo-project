export const icHome = require('../assets/icons/ic_home.png');
export const icOrder = require('../assets/icons/ic_order.png');
export const icWallet = require('../assets/icons/ic_wallet.png');
export const icStore = require('../assets/icons/ic_store.png');
export const icUser = require('../assets/icons/ic_user.png');
export const icCreditCard = require('../assets/icons/ic_credit_card.png');
const icSearch = require('../assets/icons/ic_search.png');
const icClose = require('../assets/icons/ic_close.png');
const icLove = require('../assets/icons/ic_love.png');

const Icons = {
  icHome,
  icOrder,
  icWallet,
  icStore,
  icUser,
  icCreditCard,
  icSearch,
  icClose,
  icLove
};

export default Icons;
