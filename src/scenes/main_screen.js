import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  ACCOUNT_TAB_LABEL,
  HOME_TAB_LABEL,
  ORDER_TAB_LABEL,
  STORE_TAB_LABEL,
  WALLET_TAB_LABEL,
} from '../constants/routing';
import BottomTabbar from '../components/tabbar/bottom_tabbar';
import HomeScreen from './home_screen';
import {Text, View} from 'react-native';
import Header from '../components/header/header';
import OrderScreen from './order_screen';
import TopupScreen from './top_up_screen';
import StoreScreen from './store_screen';
import AccountScreen from './account_screen';

const Tab = createBottomTabNavigator();

// const PageStack = createStackNavigator();

export default function MainScreen() {
  return (
    <Tab.Navigator
      keyExtractor={'TabScreen'}
      tabBar={props => BottomTabbar({...props})}>
      <Tab.Screen
        keyExtractor={HOME_TAB_LABEL}
        name={HOME_TAB_LABEL}
        component={HomeScreen}
      />
      <Tab.Screen
        keyExtractor={ORDER_TAB_LABEL}
        name={ORDER_TAB_LABEL}
        component={OrderScreen}
      />
      <Tab.Screen
        keyExtractor={WALLET_TAB_LABEL}
        name={WALLET_TAB_LABEL}
        component={TopupScreen}
      />
      <Tab.Screen
        keyExtractor={STORE_TAB_LABEL}
        name={STORE_TAB_LABEL}
        component={StoreScreen}
      />
      <Tab.Screen
        keyExtractor={ACCOUNT_TAB_LABEL}
        name={ACCOUNT_TAB_LABEL}
        component={AccountScreen}
      />
    </Tab.Navigator>
  );
}
