import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from '../constants/colors';

export default function TopupScreen() {
  return (
    <View style={styles.container}>
      <Text>Topup screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: Colors.backgroundColor,
  },
});
