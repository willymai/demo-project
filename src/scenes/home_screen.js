import React, {useState} from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import Category from '../components/home_screen/category';
import Collection from '../components/home_screen/collection';
import DeliveryMethod from '../components/home_screen/delivery_method';
import ListBanner from '../components/home_screen/list_banner';
import Popular from '../components/home_screen/popular';
import Voucher from '../components/home_screen/voucher';
import StyledInput from '../components/input/styled_input';
import ModalFoodDetail from '../components/modal/modal_food_detail';
import Colors from '../constants/colors';
import Icons from '../constants/icons';

export default function HomeScreen() {
  const [showModal, setShowModal] = useState(false);
  const handlePress = () => {
    setShowModal(true);
  };

  return (
    // <SafeAreaView>
    <>
      <ScrollView style={styles.container}>
        <View style={styles.inner}>
          <DeliveryMethod />
          <View style={{paddingBottom: 20}}>
            <StyledInput
              placeholder="Tìm kiếm"
              placeholderTextColor={Colors.text.regular}
              prefix={
                <View style={{paddingLeft: 22}}>
                  <Image source={Icons.icSearch} />
                </View>
              }
            />
          </View>
          <ListBanner />
          <Popular onPress={handlePress} />
          <Voucher onPress={handlePress} />
          <Collection onPress={handlePress} />
          <Category onPress={handlePress} />
        </View>
      </ScrollView>
      <ModalFoodDetail
        isVisible={showModal}
        onClose={() => setShowModal(false)}
      />
      {/* <BottomSheet
        ref={sheetRef}
        snapPoints={[450, 300, 0]}
        borderRadius={10}
        renderContent={() => (
          <View>
            <Text>asd</Text>
          </View>
        )}
      /> */}
    </>
    // </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.backgroundColor,
    // flexGrow: 1,
    // height: '100%',
  },
  inner: {
    padding: 20,
  },
});
