module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'eslint:recommended',
    'plugin:react/recommended',
  ],
  rules: {
    'no-unused-vars': 'warn',
    'react/prop-types': [0],
    'react/jsx-key': [0],
    'react-native/no-inline-styles': [0],
  },
};
